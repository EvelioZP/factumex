/*
 Navicat Premium Data Transfer

 Source Server         : Examen
 Source Server Type    : Oracle
 Source Server Version : 190000 (Oracle Database 19c Enterprise Edition Release 19.0.0.0.0 - Production)
 Source Host           : localhost:1521
 Source Schema         : HR

 Target Server Type    : Oracle
 Target Server Version : 190000 (Oracle Database 19c Enterprise Edition Release 19.0.0.0.0 - Production)
 File Encoding         : 65001

 Date: 04/12/2022 01:56:06
*/


-- ----------------------------
-- Table structure for EMPLOYEES
-- ----------------------------
DROP TABLE "EMPLOYEES";
CREATE TABLE "EMPLOYEES" (
  "ID" NUMBER VISIBLE DEFAULT "HR"."ISEQ$$_80066".nextval NOT NULL,
  "GENDER_ID" NUMBER VISIBLE,
  "JOB_ID" NUMBER VISIBLE,
  "NAME" VARCHAR2(255 BYTE) VISIBLE,
  "LAST_NAME" VARCHAR2(255 BYTE) VISIBLE,
  "BIRTHDATE" DATE VISIBLE
)
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of EMPLOYEES
-- ----------------------------
COMMIT;
COMMIT;

-- ----------------------------
-- Table structure for EMPLOYEE_WORKED_HOURS
-- ----------------------------
DROP TABLE "EMPLOYEE_WORKED_HOURS";
CREATE TABLE "EMPLOYEE_WORKED_HOURS" (
  "ID" NUMBER VISIBLE DEFAULT "HR"."ISEQ$$_80069".nextval NOT NULL,
  "EMPLOYEE_ID" NUMBER VISIBLE,
  "WORKED_HOURS" NUMBER VISIBLE,
  "WORKED_DATE" DATE VISIBLE
)
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of EMPLOYEE_WORKED_HOURS
-- ----------------------------
COMMIT;
COMMIT;

-- ----------------------------
-- Table structure for GENDERS
-- ----------------------------
DROP TABLE "GENDERS";
CREATE TABLE "GENDERS" (
  "ID" NUMBER VISIBLE DEFAULT "HR"."ISEQ$$_80063".nextval NOT NULL,
  "NAME" VARCHAR2(255 BYTE) VISIBLE
)
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of GENDERS
-- ----------------------------
COMMIT;
COMMIT;

-- ----------------------------
-- Table structure for JOBS
-- ----------------------------
DROP TABLE "JOBS";
CREATE TABLE "JOBS" (
  "ID" NUMBER VISIBLE DEFAULT "HR"."ISEQ$$_80060".nextval NOT NULL,
  "NAME" VARCHAR2(255 BYTE) VISIBLE,
  "SALARY" NUMBER(9,2) VISIBLE
)
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of JOBS
-- ----------------------------
COMMIT;
COMMIT;

-- ----------------------------
-- Sequence structure for DEPARTMENTS_SEQ
-- ----------------------------
DROP SEQUENCE "DEPARTMENTS_SEQ";
CREATE SEQUENCE "DEPARTMENTS_SEQ" MINVALUE 1 MAXVALUE 9990 INCREMENT BY 10 NOCACHE;

-- ----------------------------
-- Sequence structure for EMPLOYEES_SEQ
-- ----------------------------
DROP SEQUENCE "EMPLOYEES_SEQ";
CREATE SEQUENCE "EMPLOYEES_SEQ" MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 NOCACHE;

-- ----------------------------
-- Sequence structure for ISEQ$$_80060
-- ----------------------------
DROP SEQUENCE "ISEQ$$_80060";
CREATE SEQUENCE "ISEQ$$_80060" MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 CACHE 20;

-- ----------------------------
-- Sequence structure for ISEQ$$_80063
-- ----------------------------
DROP SEQUENCE "ISEQ$$_80063";
CREATE SEQUENCE "ISEQ$$_80063" MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 CACHE 20;

-- ----------------------------
-- Sequence structure for ISEQ$$_80066
-- ----------------------------
DROP SEQUENCE "ISEQ$$_80066";
CREATE SEQUENCE "ISEQ$$_80066" MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 CACHE 20;

-- ----------------------------
-- Sequence structure for ISEQ$$_80069
-- ----------------------------
DROP SEQUENCE "ISEQ$$_80069";
CREATE SEQUENCE "ISEQ$$_80069" MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 CACHE 20;

-- ----------------------------
-- Sequence structure for LOCATIONS_SEQ
-- ----------------------------
DROP SEQUENCE "LOCATIONS_SEQ";
CREATE SEQUENCE "LOCATIONS_SEQ" MINVALUE 1 MAXVALUE 9900 INCREMENT BY 100 NOCACHE;

-- ----------------------------
-- Primary Key structure for table EMPLOYEES
-- ----------------------------
ALTER TABLE "EMPLOYEES" ADD CONSTRAINT "EMPLOYEES_PK_ID" PRIMARY KEY ("ID");

-- ----------------------------
-- Checks structure for table EMPLOYEES
-- ----------------------------
ALTER TABLE "EMPLOYEES" ADD CONSTRAINT "SYS_C0012459" CHECK ("ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Primary Key structure for table EMPLOYEE_WORKED_HOURS
-- ----------------------------
ALTER TABLE "EMPLOYEE_WORKED_HOURS" ADD CONSTRAINT "EMPLOYEE_WORKED_HOURS_PK_ID" PRIMARY KEY ("ID");

-- ----------------------------
-- Checks structure for table EMPLOYEE_WORKED_HOURS
-- ----------------------------
ALTER TABLE "EMPLOYEE_WORKED_HOURS" ADD CONSTRAINT "SYS_C0012463" CHECK ("ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Primary Key structure for table GENDERS
-- ----------------------------
ALTER TABLE "GENDERS" ADD CONSTRAINT "GENDERS_PK_ID" PRIMARY KEY ("ID");

-- ----------------------------
-- Checks structure for table GENDERS
-- ----------------------------
ALTER TABLE "GENDERS" ADD CONSTRAINT "SYS_C0012457" CHECK ("ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Primary Key structure for table JOBS
-- ----------------------------
ALTER TABLE "JOBS" ADD CONSTRAINT "JOBS_PK_ID" PRIMARY KEY ("ID");

-- ----------------------------
-- Checks structure for table JOBS
-- ----------------------------
ALTER TABLE "JOBS" ADD CONSTRAINT "SYS_C0012455" CHECK ("ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Foreign Keys structure for table EMPLOYEES
-- ----------------------------
ALTER TABLE "EMPLOYEES" ADD CONSTRAINT "GENDER_ID_FK" FOREIGN KEY ("GENDER_ID") REFERENCES "GENDERS" ("ID") NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "EMPLOYEES" ADD CONSTRAINT "JOB_ID_FK" FOREIGN KEY ("JOB_ID") REFERENCES "JOBS" ("ID") NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Foreign Keys structure for table EMPLOYEE_WORKED_HOURS
-- ----------------------------
ALTER TABLE "EMPLOYEE_WORKED_HOURS" ADD CONSTRAINT "EMPLOYEE_ID_FK" FOREIGN KEY ("EMPLOYEE_ID") REFERENCES "EMPLOYEES" ("ID") NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
