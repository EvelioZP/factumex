package settings;


import jakarta.enterprise.context.RequestScoped;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@Path("")
@RequestScoped
public class HelloWorldResource {

    @GET
    @Produces(MediaType.TEXT_HTML)
    public String doGet() {
        return "<!DOCTYPE html><html lang=\"es-MX\"> <head> <meta charset=\"UTF-8\"/> <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"/> <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>  <title>Examen</title> <style>body div{display: flex; justify-content: center; align-items: center; height: 98vh; width: 98vw; background-color: #fff;}</style> </head> <body> <div> <h1>API Examen</h1> </div></body></html>";
    }
    
}

