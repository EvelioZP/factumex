package controller;

import jakarta.enterprise.context.RequestScoped;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import models.facade.Facade;
import models.pojos.Employee;

@Path("employee")
@RequestScoped
public class EmployeeServices extends Facade {

    @Path("save")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response Save(Employee employee) {
        return Response.ok(facadeEmployeeSave(employee)).build();
    }
    
    
    @Path("job/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response Get(@PathParam("id") int id) {
        return Response.ok(facadeEmployeeList(id)).build();
    }
    
    @Path("list")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response GetEmployee(String request) {
         return Response.ok(facadeEmployeeListEmp(request)).build();
    }

    @Path("worked/hours")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response GetEmployeeHours(String request) {
         return Response.ok(facadeEmployeeWorkeHours(request)).build();
    }
    
    
    @Path("worked/pay")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response GetEmployeeHoursPay(String request) {
         return Response.ok(facadeEmployeeWorkeHoursPay(request)).build();
    }

}
