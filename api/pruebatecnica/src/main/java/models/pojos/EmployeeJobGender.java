package models.pojos;

import java.time.LocalDate;


public class EmployeeJobGender extends Employee {
    
    private Gender genders;
    private Job job;

    public EmployeeJobGender() {
    }

    public EmployeeJobGender(Gender genders, Job job) {
        this.genders = genders;
        this.job = job;
    }

    public EmployeeJobGender(Gender genders, Job job, int id, int gender_id, int job_id, String name, String last_name, LocalDate birthdate) {
        super(id, gender_id, job_id, name, last_name, birthdate);
        this.genders = genders;
        this.job = job;

    }
    
    public Gender getGenders() {
        return genders;
    }

    public void setGenders(Gender genders) {
        this.genders = genders;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    @Override
    public String toString() {
        return "EmployeeJobGender{" + "genders=" + genders + ", job=" + job + '}';
    }
    
    
    
}
