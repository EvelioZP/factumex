package models.pojos;

import java.time.LocalDate;

public class Employee {

    private int id;
    private int gender_id;
    private int job_id;
    private String name;
    private String last_name;
    private LocalDate birthdate;

    public Employee() {
    }

    public Employee(int id, int gender_id, int job_id, String name, String last_name, LocalDate birthdate) {
        this.id = id;
        this.gender_id = gender_id;
        this.job_id = job_id;
        this.name = name;
        this.last_name = last_name;
        this.birthdate = birthdate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGender_id() {
        return gender_id;
    }

    public void setGender_id(int gender_id) {
        this.gender_id = gender_id;
    }

    public int getJob_id() {
        return job_id;
    }

    public void setJob_id(int job_id) {
        this.job_id = job_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
    }

    @Override
    public String toString() {
        return "employee{"
                + this.gender_id + " "
                + this.job_id + " "
                + this.name + " "
                + this.last_name + " "
                + this.birthdate + "}";

    }

}
