package models.oracle;

import java.sql.*;

public class Conexion {

    // *I would use ORM, but for practical testing purposes I will use
    // PreparedStatement
    // *Is usually stored in a property file
    private static final String URL = "jdbc:oracle:thin:@localhost:1521/orcl";
    private static final String USER = "hr";
    private static final String PASS = "oracle";

    static Connection getConnection() {
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            return DriverManager.getConnection(URL, USER, PASS);
        } catch (ClassNotFoundException | SQLException e) {
            return null;
        }

    }

    static void close(ResultSet rs, Statement st, Connection cn) {
        try {
            rs.close();
        } catch (SQLException e) {
            e.getMessage();
        } catch (Exception e) {
            e.getMessage();
        }

        try {
            st.close();
        } catch (SQLException e) {
            e.getMessage();
        } catch (Exception e) {
            e.getMessage();
        }

        try {
            cn.close();
        } catch (SQLException e) {
            e.getMessage();
        } catch (Exception e) {
            e.getMessage();
        }
    }

    static void rollback(Connection cn) {
        try {
            cn.rollback();
        } catch (SQLException e) {
            e.getMessage();
        } catch (Exception e) {
            e.getMessage();
        }
    }

}
