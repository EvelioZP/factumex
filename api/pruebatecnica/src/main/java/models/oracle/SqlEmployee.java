package models.oracle;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import models.pojos.Employee;
import models.pojos.EmployeeJobGender;

public class SqlEmployee extends Conexion {

    public SqlEmployee() {
        super();
    }

    // create method to get all employees prepared statement and return a list of
    // employees
    // create method to get employee by id prepared statement and return a employee
    public static void getEmployee() {
        Connection conn = getConnection();
        PreparedStatement sentencia = null;
        ResultSet rs = null;
        try {
            String consulta = "SELECT * FROM HR.JOBS";
            sentencia = conn.prepareStatement(consulta);
            rs = sentencia.executeQuery();
            while (rs != null && rs.next()) {
                System.out.println(rs.getString("JOB_ID"));
            }
        } catch (SQLException ex) {
            rollback(conn);
        } finally {
            close(rs, sentencia, conn);
        }

    }

    public static List<EmployeeJobGender> getListEmployees(int id) {
        Connection conn = getConnection();
        PreparedStatement sentencia = null;
        ResultSet rs = null;
        List<EmployeeJobGender> employees = new ArrayList<>();
        try {
            String consulta = "SELECT * FROM HR.EMPLOYEES WHERE JOB_ID = ?";
            sentencia = conn.prepareStatement(consulta);
            sentencia.setInt(1, id);
            rs = sentencia.executeQuery();
            while (rs != null && rs.next()) {
                LocalDate localDate = rs.getDate("BIRTHDATE").toLocalDate();
                EmployeeJobGender employeeJobGender = new EmployeeJobGender();
                employeeJobGender.setId(rs.getInt("ID"));
                employeeJobGender.setGender_id(rs.getInt("GENDER_ID"));
                employeeJobGender.setJob_id(rs.getInt("JOB_ID"));
                employeeJobGender.setName(rs.getString("NAME"));
                employeeJobGender.setLast_name(rs.getString("LAST_NAME"));
                employeeJobGender.setBirthdate(localDate);

                employeeJobGender.setJob(SqlJobs.getJob(employeeJobGender.getJob_id()));
                employeeJobGender.setGenders(SqlGenders.getGender(employeeJobGender.getGender_id()));
                employees.add(employeeJobGender);
            }
        } catch (SQLException ex) {
            rollback(conn);
        } finally {
            close(rs, sentencia, conn);
        }
        return employees;
    }

    public static EmployeeJobGender getEmployeesDate(int id , String date1 , String date2) {
        Connection conn = getConnection();
        PreparedStatement sentencia = null;
        ResultSet rs = null;
        EmployeeJobGender employeeJobGender = null;
        try {
            String consulta = "SELECT * FROM HR.EMPLOYEES WHERE JOB_ID = ? AND BIRTHDATE >= to_date(? , 'YYYY-MM-DD') AND BIRTHDATE <= to_date(? , 'YYYY-MM-DD')";
            sentencia = conn.prepareStatement(consulta);
            sentencia.setInt(1, id);
            sentencia.setString(2, date1);
            sentencia.setString(3, date2);
            rs = sentencia.executeQuery();
            while (rs != null && rs.next()) {
                LocalDate localDate = rs.getDate("BIRTHDATE").toLocalDate();
                employeeJobGender = new EmployeeJobGender();
                employeeJobGender.setId(rs.getInt("ID"));
                employeeJobGender.setGender_id(rs.getInt("GENDER_ID"));
                employeeJobGender.setJob_id(rs.getInt("JOB_ID"));
                employeeJobGender.setName(rs.getString("NAME"));
                employeeJobGender.setLast_name(rs.getString("LAST_NAME"));
                employeeJobGender.setBirthdate(localDate);

                employeeJobGender.setJob(SqlJobs.getJob(employeeJobGender.getJob_id()));
                employeeJobGender.setGenders(SqlGenders.getGender(employeeJobGender.getGender_id()));
                
            }
        } catch (SQLException ex) {
            rollback(conn);
        } finally {
            close(rs, sentencia, conn);
        }
        return employeeJobGender;
    }
    
    public static EmployeeJobGender getEmployeeJobGender(int id) {
        Connection conn = getConnection();
        PreparedStatement sentencia = null;
        ResultSet rs = null;
        EmployeeJobGender employeeJobGender = null;
        try {
            String consulta = "SELECT * FROM HR.EMPLOYEES WHERE ID = ?";
            sentencia = conn.prepareStatement(consulta);
            sentencia.setInt(1, id);
            rs = sentencia.executeQuery();
            while (rs != null && rs.next()) {
                LocalDate localDate = rs.getDate("BIRTHDATE").toLocalDate();
                employeeJobGender = new EmployeeJobGender();
                employeeJobGender.setId(rs.getInt("ID"));
                employeeJobGender.setGender_id(rs.getInt("GENDER_ID"));
                employeeJobGender.setJob_id(rs.getInt("JOB_ID"));
                employeeJobGender.setName(rs.getString("NAME"));
                employeeJobGender.setLast_name(rs.getString("LAST_NAME"));
                employeeJobGender.setBirthdate(localDate);

                employeeJobGender.setJob(SqlJobs.getJob(employeeJobGender.getJob_id()));
                employeeJobGender.setGenders(SqlGenders.getGender(employeeJobGender.getGender_id()));
                
            }
        } catch (SQLException ex) {
            rollback(conn);
        } finally {
            close(rs, sentencia, conn);
        }
        return employeeJobGender;
    }

    public static boolean getEmployeeName(String name, String last_name) {
        Connection conn = getConnection();
        PreparedStatement sentencia = null;
        ResultSet rs = null;
        boolean result = false;
        try {
            String consulta = "SELECT * FROM HR.EMPLOYEES WHERE NAME = ? and LAST_NAME = ?";
            sentencia = conn.prepareStatement(consulta);
            sentencia.setString(1, name);
            sentencia.setString(2, last_name);
            rs = sentencia.executeQuery();
            while (rs != null && rs.next()) {
                result = true;
            }
        } catch (SQLException ex) {
            rollback(conn);
        } finally {
            close(rs, sentencia, conn);
        }
        return result;
    }

    public static long setEmployee(Employee employee) {
        Connection conn = getConnection();
        PreparedStatement sentencia = null;
        ResultSet rs = null;
        long id = 0;
        try {
            String consulta = "INSERT INTO HR.EMPLOYEES (GENDER_ID, JOB_ID, NAME, LAST_NAME, BIRTHDATE) VALUES (?,?,?,?,?)";
            sentencia = conn.prepareStatement(consulta, new String[]{"ID"});
            sentencia.setInt(1, employee.getGender_id());
            sentencia.setInt(2, employee.getJob_id());
            sentencia.setString(3, employee.getName());
            sentencia.setString(4, employee.getLast_name());
            sentencia.setDate(5, java.sql.Date.valueOf(employee.getBirthdate()));
            sentencia.executeUpdate();
            rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getInt(1);
            }

        } catch (SQLException ex) {
            rollback(conn);
        } finally {
            close(rs, sentencia, conn);
        }
        return id;
    }

}
