package models.oracle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import static models.oracle.Conexion.close;
import static models.oracle.Conexion.getConnection;
import static models.oracle.Conexion.rollback;
import models.pojos.EmployeeWorkeHours;

public class SqlEmployeeWorkeHours {

    public static List<EmployeeWorkeHours> getEmployeeWorkeHoursID(int i, String date1, String date2) {
        Connection conn = getConnection();
        PreparedStatement sentencia = null;
        ResultSet rs = null;
        List<EmployeeWorkeHours> workeHours = new ArrayList<>();
        try {
            String consulta = "SELECT * FROM HR.EMPLOYEE_WORKED_HOURS WHERE EMPLOYEE_ID = ? AND  WORKED_DATE >= to_date(? , 'YYYY-MM-DD') AND WORKED_DATE <= to_date(? , 'YYYY-MM-DD')";
            sentencia = conn.prepareStatement(consulta);
            sentencia.setInt(1, i);
            sentencia.setString(2, date1);
            sentencia.setString(3, date2);

            rs = sentencia.executeQuery();
            while (rs != null && rs.next()) {
                EmployeeWorkeHours workeHour = new EmployeeWorkeHours(rs.getInt("ID"), rs.getInt("EMPLOYEE_ID"), rs.getInt("WORKED_HOURS"), rs.getDate("WORKED_DATE").toLocalDate());
                workeHours.add(workeHour);
            }
        } catch (SQLException ex) {
            rollback(conn);
        } finally {
            close(rs, sentencia, conn);
        }
        return workeHours;
    }

}
