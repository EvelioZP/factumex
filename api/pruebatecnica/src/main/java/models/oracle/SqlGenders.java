
package models.oracle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import static models.oracle.Conexion.close;
import static models.oracle.Conexion.getConnection;
import static models.oracle.Conexion.rollback;
import models.pojos.Gender;

public class SqlGenders {
    
        public static Gender getGender(int i) {
        Gender gender = null;
        Connection conn = getConnection();
        PreparedStatement sentencia = null;
        ResultSet rs = null;
        try {
            String consulta = "SELECT * FROM HR.GENDERS WHERE ID = ?";
            sentencia = conn.prepareStatement(consulta);
            sentencia.setInt(1, i);
            rs = sentencia.executeQuery();
            while (rs != null && rs.next()) {
                gender = new Gender(rs.getInt("ID"), rs.getString("NAME"));
            }
        } catch (SQLException ex) {
            rollback(conn);
        } finally {
            close(rs, sentencia, conn);
        }
        return gender;
    }
    
}
