package models.oracle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import models.pojos.Job;

public class SqlJobs extends Conexion {

    public static List<Job> getJobs() {
        List<Job> jobs = new ArrayList<>();
        Connection conn = getConnection();
        PreparedStatement sentencia = null;
        ResultSet rs = null;
        try {
            String consulta = "SELECT * FROM HR.JOBS";
            sentencia = conn.prepareStatement(consulta);
            rs = sentencia.executeQuery();
            while (rs != null && rs.next()) {
                jobs.add(new Job(rs.getInt("ID"), rs.getString("NAME"), rs.getBigDecimal("SALARY")));
            }
        } catch (SQLException ex) {
            rollback(conn);
        } finally {
            close(rs, sentencia, conn);
        }
        return jobs;
    }

    public static Job getJob(int i) {
        Job job = null;
        Connection conn = getConnection();
        PreparedStatement sentencia = null;
        ResultSet rs = null;
        try {
            String consulta = "SELECT * FROM HR.JOBS WHERE ID = ?";
            sentencia = conn.prepareStatement(consulta);
            sentencia.setInt(1, i);
            rs = sentencia.executeQuery();
            while (rs != null && rs.next()) {
                job = new Job(rs.getInt("ID"), rs.getString("NAME"), rs.getBigDecimal("SALARY"));
            }
        } catch (SQLException ex) {
            rollback(conn);
        } finally {
            close(rs, sentencia, conn);
        }
        return job;
    }

    public static boolean getJobID(int i) {
        Connection conn = getConnection();
        PreparedStatement sentencia = null;
        ResultSet rs = null;
        boolean result = false;
        try {
            String consulta = "SELECT * FROM HR.JOBS WHERE ID = ?";
            sentencia = conn.prepareStatement(consulta);
            sentencia.setInt(1, i);
            rs = sentencia.executeQuery();
            while (rs != null && rs.next()) {
                result = true;
            }
        } catch (SQLException ex) {
            rollback(conn);
        } finally {
            close(rs, sentencia, conn);
        }
        return result;
    }

}
