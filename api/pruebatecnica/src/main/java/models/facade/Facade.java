package models.facade;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import models.oracle.SqlEmployee;
import models.oracle.SqlEmployeeWorkeHours;
import models.oracle.SqlJobs;
import models.pojos.Employee;
import models.pojos.EmployeeJobGender;
import models.pojos.EmployeeWorkeHours;
import org.json.JSONArray;
import org.json.JSONObject;
import static org.json.JSONObject.NULL;

public class Facade {

    public String facadeEmployeeSave(Employee employee) {

        JSONObject json = new JSONObject();
        if (!SqlEmployee.getEmployeeName(employee.getName(), employee.getLast_name()) && SqlJobs.getJobID(employee.getJob_id()) && calculateAge(employee.getBirthdate()) >= 18) {
            json.put("id", SqlEmployee.setEmployee(employee));
            json.put("success", true);
        } else {
            json.put("id", NULL);
            json.put("success", false);
        }
        return json.toString();
    }

    public String facadeEmployeeList(int id) {
        List<EmployeeJobGender> employees = SqlEmployee.getListEmployees(id);
        JSONObject json = new JSONObject();

        employees.sort((o1, o2)
                -> o1.getLast_name().compareTo(
                        o2.getLast_name()));

        if (employees.isEmpty()) {
            json.put("employees", NULL);
            json.put("success", false);
        } else {
            json.put("success", true);
            json.put("employees", ListEmJSONArray(employees));
        }
        return json.toString();
    }

    public String facadeEmployeeListEmp(String request) {
        JSONObject json = new JSONObject(request);
        List<EmployeeJobGender> employees = new ArrayList<>();
        listFuture(json, employees);
        json = new JSONObject();
        if (employees.isEmpty()) {
            json.put("employees", NULL);
            json.put("success", false);
        } else {
            json.put("success", true);
            json.put("employees", ListEmJSONArray(employees));
        }
        return json.toString();
    }

    public String facadeEmployeeWorkeHours(String request) {
        JSONObject json = new JSONObject(request);
        int id = json.getInt("employee_id");
        String start_date = json.getString("start_date");
        String end_date = json.getString("end_date");

        List<EmployeeWorkeHours> WorkeHours = SqlEmployeeWorkeHours.getEmployeeWorkeHoursID(id, start_date, end_date);
        json = new JSONObject();
        if (WorkeHours == null || WorkeHours.isEmpty()) {
            json.put("total_worked_hours", NULL);
            json.put("success", false);
        } else {
            int sum = WorkeHours.stream().filter(o -> o.getWorked_hours() > 10).mapToInt(o -> o.getWorked_hours()).sum();
            json.put("total_worked_hours", sum);
            json.put("success", true);
        }
        return json.toString();
    }

    public String facadeEmployeeWorkeHoursPay(String request) {
        JSONObject json = new JSONObject(request);
        int id = json.getInt("employee_id");
        String start_date = json.getString("start_date");
        String end_date = json.getString("end_date");

        List<EmployeeWorkeHours> WorkeHours = SqlEmployeeWorkeHours.getEmployeeWorkeHoursID(id, start_date, end_date);
        EmployeeJobGender employee = SqlEmployee.getEmployeeJobGender(id);
        json = new JSONObject();
     
        if (employee == null ||  WorkeHours == null ||  WorkeHours.isEmpty()) {
            json.put("total_worked_hours", NULL);
            json.put("success", false);
        } else {
            int sum = WorkeHours.stream().filter(o -> o.getWorked_hours() > 10).mapToInt(o -> o.getWorked_hours()).sum();
            json.put("total_worked_hours", employee.getJob().getSalary().multiply(new BigDecimal(sum)));
            json.put("success", true);
        }
        return json.toString();
    }

    private void listFuture(JSONObject json, List<EmployeeJobGender> employees) {
        JSONArray listId = json.getJSONArray("employee_id");
        String start_date = json.getString("start_date");
        String end_date = json.getString("end_date");

        for (int i = 0; i < listId.length(); i++) {
            EmployeeJobGender employee = SqlEmployee.getEmployeesDate(listId.getInt(i), start_date, end_date);
            if (employee != null) {
                employees.add(employee);
            }
        }
    }

    private JSONArray ListEmJSONArray(List<EmployeeJobGender> employees) {

        JSONArray array = new JSONArray();
        for (EmployeeJobGender employee : employees) {
            JSONObject emp = new JSONObject().put("id", employee.getId()).put("name", employee.getName()).put("last_name", employee.getLast_name()).put("birthdate", employee.getBirthdate());
            emp.put("job", new JSONObject().put("id", employee.getJob().getId()).put("name", employee.getJob().getName()).put("salary", employee.getJob().getSalary()));
            emp.put("gender", new JSONObject().put("id", employee.getGenders().getId()).put("name", employee.getGenders().getName()));
            array.put(emp);
        }

        return array;
    }

    private int calculateAge(LocalDate birthDate) {
        LocalDate currentDate = LocalDate.now();
        if ((birthDate != null) && (currentDate != null)) {
            return Period.between(birthDate, currentDate).getYears();
        } else {
            return 0;
        }
    }

}
